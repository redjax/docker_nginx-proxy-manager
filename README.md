# docker_nginx-proxy-manager

Template for an nginx-proxy-manager instance. Copy .env.example to .env and fill out things like passwords, edit the config.json file, then run docker-compose up -d

DON'T FORGET TO UPDATE CONFIG.JSON WITH CORRECT VALUES.

Default admin login is:
  USER: admin@example.com
  PASS: changeme
